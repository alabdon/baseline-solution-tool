import solver as opds
import logging
import matplotlib.pyplot as plt;
import numpy as np
import sys, getopt
from astroquery.vizier import Vizier

def Setup(GUI=0):
	# set up logging to file 
	logging.basicConfig(level=logging.DEBUG,format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
											datefmt='%m-%d %H:%M',filename='Baseline.log',filemode='w')
	console = logging.StreamHandler()
	console.setLevel(logging.INFO)
	formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
	console.setFormatter(formatter)
	logging.getLogger('').addHandler(console)

	print ('---------------------------------------------------------------------------------------')
	print ('                 	Welcome to the BaselinE SOlution Tool')
	print ('                                      BESOT									 ')
	print ('                                      v 0.1									 ')
	print ('')
	print ('')
	print ('')
	print ('Change Log:')
	print ('2018:			 Basic model built by J.B. Bug fixes by A.L.')
	print ('2019-01-22: GAIA fltering and 2 step solution A.L.')
	print ('2019-01-23: Fixed beams option, time dependence option, fake offsets, logging A.L.')
	print ('2019-01-28: Re-organised and streamlined')
	print ('2019-01-29: Added LIGHT term calculation for iphase mode and config file')
	print ('2019-04-18: Swash terms from iphase now an option')
	print ('2019-04-19: Visualisation now adds an ovverplotting of on-sky offsets')
	print ('2019-04-23: Added option to compare 2 solutions and return the difference')
	print ('2019-04-25: Created GUI and streamlined filtering process')
	print ('')
	print ('---------------------------------------------------------------------------------------')
	print ('')
	print ('')
	if GUI == 0:
		settings_in,settings_res = [],[]
		with open("config") as f:
			for line in f:
				line = line.partition('#')[0]
				line = (line.rstrip()).split()
				try:
					settings_in.append(line[0])
					settings_res.append(line[1])
				except:
					a=1
		return settings_in, settings_res

	

def Standard_Fit(inputFile,time_mode,beam_mode,ec_mode,sas_mode,sac_mode,dof_mode,outFile,outPlot,File,old_solution):
	logging.info ('Starting the fitting process.........')
	logging.info ('Fitting x,y,z position acording to '+dof_mode+' method')
	print ('-----------------------------------------------------------------')
	#Now start the fitting
	model, new_dof, params, aux, offsets = opds.load_model (inputFile,fit='dl',dof=dof_mode, zcoord='consider_index', Ltime=time_mode, ec_mode=ec_mode,sas_mode=sas_mode,sac_mode=sac_mode);
	# Init vector, which containss the numerical
	# values of the parameters, some of them shall be
	# pre-defined because they are not fitted.
	#dof_mode = dof
	vector, dof = opds.init_vector (params, mode=beam_mode, Ltime=time_mode, dof=dof_mode, file=File);
	# Solve
	output, error, residual, LIGHT_terms = opds.solve (model, params, vector, dof, old_solution, dof_mode=dof_mode);
	# Print
	opds.dump_vector_as_chara (params, output,new_dof, LIGHT_terms, dof_mode=dof_mode, name=outFile);
	# Plots
	rms_a = opds.plot_residuals (residual, aux, offsets, dof_mode=dof_mode);
	#plt.savefig(outPlot);
	#plt.clf()
	#plt.show ();
	return rms_a


def Filter_Data2(inputFile,outputFile,Extra,SepLim=1.0,ProLim=500.,MagLim=0.5):
 f = open('starlist','r')
 Checklist = f.readlines()
 f.close()

 f = open(inputFile,'r')
 data = f.readlines()
 f.close()
 CHARA_star = []
 for i,line in enumerate(data):
  if 'Manual' in line:
   line = line.split()
   CHARA_star.append(float(line[11].strip('#')))
 logging.info ('Starting the filtering process')
 logging.info ('Connecting to Vizier, requires internet connection')
 CHARA_cond = list(set(CHARA_star))
 Bad_stars = []
 #for i in CHARA_cond:
 for j, line in enumerate(Checklist):
  if 'CHARA' in line:
   line = line.split()
   if float(line[1]) in CHARA_cond:
    ID = Checklist[j-1].split()
    ID = ID[1]
    PM = 1.
    for l in Checklist[j:j+15]:
     l = l.split()
     if l[0] == 'RA':
      RA = float(l[1])*15+float(l[2])/4.+float(l[3])/240.
     if l[0] == 'DEC':
      DEC = float(l[1])+float(l[2])/60.+float(l[3])/3600.
     if l[0] == 'PMRA':
      pm1 = float(l[1])
     if l[0] == 'PMDEC':
      pm2 = float(l[1])
      PM = (pm1**2+pm2**2)
     if l[0] == 'VMAG':
      V = float(l[1])
    #print RA, DEC, PM, V
    v = Vizier(columns=['_r','RAJ2000','DEJ2000','Gmag','pmRA','pmDE'])
    result = v.query_object(ID, catalog=["I/345"])
    dist = []
    for i in range(0,len(result['I/345/gaia2']),1):
     dist.append(float(result['I/345/gaia2'][i][0]))
    val = dist.index(np.min(dist))
    RA_g = float(result['I/345/gaia2'][val][6])
    DE_g = float(result['I/345/gaia2'][val][7])
    G_g = float(result['I/345/gaia2'][val][3])
    PM_g = float(result['I/345/gaia2'][val][4])**2 + float(result['I/345/gaia2'][val][5])**2
    PM_g = np.sqrt(PM_g)
    if np.isnan(PM_g):
     PM_g = 1.0

    #print RA_g, DE_g, PM_g, G_g

    if RA-SepLim >= RA_g >= RA+SepLim or np.abs(DEC)-SepLim >= np.abs(DE_g) or np.abs(DE_g) >= np.abs(DEC)+SepLim or PM_g >= ProLim or V -MagLim >= G_g and V+MagLim <= G_g:
     logging.warning ('Rejecting star, did not meet specified criteria')
     Bad_stars.append(float(line[1]))

 deleter = []
 for i,line in enumerate(data):
  if 'Manual' in line:
   line = line.split()
   if float(line[11].strip('#')) in Bad_stars:
    deleter.append(i-1)
    deleter.append(i)
    deleter.append(i+1)
   if not 'MIRC' in line and Extra == 'MIRC_only':
    deleter.append(i-1)
    deleter.append(i)
    deleter.append(i+1)

 New_data = np.delete(data,deleter)

 writer = []
 file = open(outputFile,"w")
 logging.info ('Writing new observation data file :'+ outputFile)
 for i in New_data:
  writer.append('{}'.format(i))

 file.writelines(writer)
