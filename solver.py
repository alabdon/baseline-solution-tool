import numpy as np;
from astropy.stats import sigma_clipped_stats;
import os
import matplotlib.pyplot as plt;
import matplotlib as mpl
mpl.rcParams['image.interpolation'] = 'nearest';
mpl.rcParams['axes.grid'] = True;
mpl.rcParams['legend.framealpha'] = 0.5;
import logging
verbose = False;

def myround(x, base=30):
    return int(base * round(float(x)/base))

def get_mjd (s):
    ''' Get MJD from Time string. Deal with crappy format '''
    from astropy.time import Time;
    try:
        s = list(s); s[10] = 'T'; s[13] = ':'; s[16] = ':'; s = ''.join(s);
        return Time(s).mjd;
    except:
        return 0.0;

def set_model (model, params, name, obs, value):
    '''
    Set value in correct place in the matrix 
    defining the MODEL (nobs, nparams)

    If this parameter is not yet in the model,
    it is added to MODEL and PARAMS.
    '''
    
    # Create the column if not already in model
    if name not in params:
        if verbose: logging.info ('Add PARAMS: '+name);
        params += [name];
            
    # Set the value
    model[obs, params.index(name)] = value;

def get_aux (line, title, default):
    '''
    Parser to recover the field value in the line.
    The value is returned with the same type as the
    one of the default.
    '''

    # Check this field is on the line
    if title not in line:
        return default;

    # Grab it and convert it
    value = (line.split (title)[1].strip().split(' ')[0]);
    return type(default)(value);

def load_model (name, restrict=[], reject=[], beam='tel', fit='dl',
                dof='optimal', zcoord='consider_index', mode='beams',Ltime='None',ec_mode='None',sas_mode='None',sac_mode='None'):
    '''
    Parse the CHARA data and create the model matrix.
    This matrix is of shape MODEL (nobs, nparams)
    
    Each parameter is indentified by a a name, which
    is given in the PARAMS (nparams), with the same
    sorting as the second dimension of MODEL.

    The names of the paramers are taken from the CHARA
    OPD model file, 'telescopes_chara.txt'

    AUX (nobs) contains auxiliary data about the observations,
    in a structurated python array for convenience.
    (CHARA id, n_obs, az, elev...)

    RESTRICT=['S1','S2'], for instance, can be used to
    load only some configuration, or stars.

    REJECT=['S1','S2'], for instance, can be used to
    load only some configuration, or stars.
    '''
    logging.info ('Loading data and setting up degrees of freedom');
    new_dof = []
    if type(restrict) != list:
        raise ValueError('restrict shall be a list');
    
    if type(reject) != list:
        raise ValueError('reject shall be a list');

    # Load data as a list of lines
    name = [name] if type (name) not in (list, tuple) else name;
    lines = [];
    for n in name:
        lines += open (n, 'r').readlines();

    # Create model array as pure numpy, for performances
    nobs, nparams = len(lines)/2, 1000;
    model  = np.zeros ((int(nobs),int(nparams)));
    params = [];
    obs    = 0;
    offsets = []
    # Create auxiliary data array, as structured array for convenience
    aux = np.rec.array(np.zeros (int(nobs), dtype=[('az',float), ('elev',float),
                                              ('t1','a10'), ('t2','a10'),
                                              ('offset1',float), ('offset2',float),
                                              ('dl1',float), ('dl2',float),
                                              ('chara',int),
                                              ('BC','a20'), ('UT','a25'), ('mjd',float),
                                              ('n_obs',float)]));

    # Parse data by looping on lines
    for i,l in enumerate(lines):
        # Not a line with data
        if l[0] != 'D': continue;

            
        # Check if this line shall be discarded
        ok = True;
        for r in restrict:
            if r not in l and r not in lines[i+2]: ok = False;
        for r in reject:
            if r in l or r in lines[i+1] or r in lines[i+2]: ok = False;
        if ok is False: continue;

        # Parse auxiliary data
        d = lines[i+1];
        aux[obs].chara = get_aux (d, ' CHARA #', -1);
        aux[obs].UT = get_aux (d, ' UT ', '0000');
        aux[obs].mjd = get_mjd (aux[obs].UT);
        aux[obs].BC = get_aux (d, ' BC: ', 'Unknown_BC');
        aux[obs].n_obs = get_aux (d, ' Obs n=', 1.000273);
        aux[obs].offset1 = float(d.split()[3])*1e6;
        aux[obs].offset2 = float(d.split()[4])*1e6;
        offsets.append(float(d.split()[3])*1e6 - float(d.split()[4])*1e6);
        
        # Parse config data (telescope name, POP name, BEAM name)
        t1, t2 = l[2:4], l[4:6];
        p1, p2 = l[7], l[8];
        b1, b2 = "BEAM"+l[10], "BEAM"+l[11];
        tim = str(myround(int(aux[obs].mjd)))

        aux[obs].t1 = t1;
        aux[obs].t2 = t2;
        
        # Parse Az, Elev
        d = lines[i+2].split ();
        A = -np.array (float(d[1])) / 180. * np.pi;
        E =  np.array (float(d[2])) / 180. * np.pi;
        dl1 = float(d[3])*1e6;
        dl2 = float(d[4])*1e6;
        n_obs = aux[obs].n_obs;

        # Save here as well
        aux[obs].az   = A;
        aux[obs].elev = E;
        aux[obs].dl1 = dl1;
        aux[obs].dl2 = dl2;
        
        # Contribution of telescope X position, in vaccum
        coef = np.cos (E) * np.sin (A);
        set_model (model, params, t1+"_XOFFSET", obs, +coef);
        set_model (model, params, t2+"_XOFFSET", obs, -coef);
        
        # Contribution of telescope Y position, in vaccum
        coef = - np.cos (E) * np.cos (A);
        set_model (model, params, t1+"_YOFFSET", obs, +coef);
        set_model (model, params, t2+"_YOFFSET", obs, -coef);
        
        # Contribution of telescope Z position, in the CHARA model,
        # this is split between in vaccum and air. In the iPhase
        # model, this is entirely is vacuum
        if zcoord == 'consider_index':
            coef = + np.cos (E) * np.cos (E) / np.sin (E) - n_obs / np.sin (E);
        elif zcoord == 'iphase':
            coef = - np.sin (E);
        else:
            raise ValueError ('zcoord is not recognized');
            
        set_model (model, params, t1+"_ZOFFSET", obs, +coef);
        set_model (model, params, t2+"_ZOFFSET", obs, -coef);

        #Swash Components

        if ec_mode == 1:  #Elevation cosine swash
            coeff = - np.cos (E);
            set_model (model, params, t1+"_EC", obs, +coef);
            set_model (model, params, t2+"_EC", obs, -coef);
        if sac_mode == 1:  #Azimuthal cosine swash
            coeff = - np.cos (A) * (1 - np.sin (E) );
            set_model (model, params, t1+"_SAC", obs, +coef);
            set_model (model, params, t2+"_SAC", obs, -coef);
        if sas_mode == 1:  #Azimuthal sine swash
            coeff = - np.sin (A) * (1 - np.sin (E) );
            set_model (model, params, t1+"_SAS", obs, +coef);
            set_model (model, params, t2+"_SAS", obs, -coef);
        
        if dof == 'iphase':
            # IPHASE dof
            setup = t1+t2+p1+p2+b1+b2;
            set_model (model, params, setup, obs, +1.0);
            
        elif dof == 'optimal':

            if Ltime == 'Light':
                # Contribution of lightpath static opd, in vaccum
                set_model (model, params, t1+"_LIGHT_"+tim,   obs, +1.);
                set_model (model, params, t2+"_LIGHT_"+tim,   obs, -1.);

                 # Contribution of POP, per telescope, in vaccum
                set_model (model, params, t1+"_POP"+p1, obs, +1.);
                set_model (model, params, t2+"_POP"+p2, obs, -1.);
                
                new_dof.append("_LIGHT_"+tim)
            else:
                set_model (model, params, t1+"_LIGHT",   obs, +1.);
                set_model (model, params, t2+"_LIGHT",   obs, -1.);
                
                # Contribution of POP, per telescope, in vaccum
                set_model (model, params, t1+"_POP"+p1, obs, +1.);
                set_model (model, params, t2+"_POP"+p2, obs, -1.);
            
            # Contribution of BEAM, per telescope or days, in air
            if 'day' in beam:
                b1, b2 = b1+"_%d"%(aux[obs,4]), b2+"_%d"%(aux[obs,4]);
            #if 'tel' in beam:
            #    b1, b2 = t1+'_'+b1, t2+'_'+b2;
            set_model (model, params,t1+ "_"+b1, obs, +n_obs);
            set_model (model, params,t2+"_"+b2, obs, -n_obs);
            
            # Contribution of path of air, unfitted
            set_model (model, params, t1+"_AIRPATH", obs, +n_obs);
            set_model (model, params, t2+"_AIRPATH", obs, -n_obs);
        else:
            raise ValueError ('dof is not recognized');
            
        # Constribution of Delay Line, per telescope, unfitted
        # These are computed in vaccum by the real-time code
        if fit == 'dl':
            set_model (model, params, t1+"_DL", obs, +dl1);
            set_model (model, params, t2+"_DL", obs, -dl2);
        elif fit == 'offsets':
            set_model (model, params, t1+"_DL", obs, +aux[obs].offset1);
            set_model (model, params, t2+"_DL", obs, -aux[obs].offset2);
        elif fit == 'dl-2*offsets':
            set_model (model, params, t1+"_DL", obs, +dl1-2.*aux[obs].offset1);
            set_model (model, params, t2+"_DL", obs, -dl2+2.*aux[obs].offset2);
        elif fit == 'dl+offsets':
            set_model (model, params, t1+"_DL", obs, +dl1+aux[obs].offset1);
            set_model (model, params, t2+"_DL", obs, -dl2-aux[obs].offset2);
        else:
            raise ValueError ('fit is not recognized');

        # Next observation
        obs = obs + 1;

    # Clean model from unused params
    model = model[:,0:len(params)];

    # Clean model from unused obs
    is_valid = np.sum(model,axis=1) != 0;
    model = model[is_valid,:];
    aux   = aux[is_valid];
    new_dof = list(set(new_dof))
    logging.info ('Found %i observations with %i degrees of freedom'%model.shape);
    return model,new_dof,params,aux,offsets;
        
def init_vector (params, mode='beams', Ltime='None', dof='optimal', file=None):
    '''
    Init VECTOR (nparams), which contains the numerical values
    of the various parameters of the baseline solution.

    Actually, this is a linear problem, which is therefore
    convex: the inital guess is not relevant.

    However, some parameters should not be fitted, and are
    therefore pre-defined hardcoded.
    '''    

    # Init memory
    File_Params = []
    vector = np.zeros (len(params));

    if file == None or file == 'None':
        logging.info ('Init vector');
        if mode == 'beams' or Ltime == 'Light':
             # Some predefined quantities.
             n = [('S1_AIRPATH', 0.0), ('S2_AIRPATH', 573633.833),
                ('E1_AIRPATH', 4250500.0), ('E2_AIRPATH', 3680300.0),
                ('W1_AIRPATH', 1842354.0), ('W2_AIRPATH', 2405131.0),
                ('S1_DL', 1.0), ('S2_DL', 1.0), ('E1_DL', 1.0),
                ('E2_DL', 1.0), ('W1_DL', 1.0), ('W2_DL', 1.0),
                ('S1_BEAM1', 419584.274), ('S1_BEAM2', 210779.846),('S1_BEAM3', 0.0), ('S1_BEAM4', -210317.016),('S1_BEAM5', -420906.067), ('S1_BEAM6', -630805.373),
                ('S2_BEAM1', 419584.274), ('S2_BEAM2', 209369.004),('S2_BEAM3', 0.0), ('S2_BEAM4', -210760.772),('S2_BEAM5', -420354.843), ('S2_BEAM6', -631273.985),
                ('E1_BEAM1', 419809.997), ('E1_BEAM2', 210045.978),('E1_BEAM3', 0.0), ('E1_BEAM4', -210669.979),('E1_BEAM5', -420954.973), ('E1_BEAM6', -631390.929),
                ('E2_BEAM1', 418909.967), ('E2_BEAM2', 209645.987),('E2_BEAM3', 0.0), ('E2_BEAM4', -210769.981),('E2_BEAM5', -421355.009), ('E2_BEAM6', -631841.004),
                ('W1_BEAM1', 420409.977), ('W1_BEAM2', 210145.980),('W1_BEAM3', 0.0), ('W1_BEAM4', -210149.989),('W1_BEAM5', -420255.005), ('W1_BEAM6', -630540.967),
                ('W2_BEAM1', 419609.994), ('W2_BEAM2', 210245.997),('W2_BEAM3', 0.0), ('W2_BEAM4', -210469.961),('W2_BEAM5', -420654.982), ('W2_BEAM6', -630841.017)];
        else:
            # Some predefined quantities.
            n = [('S1_AIRPATH', 0.0), ('S2_AIRPATH', 573633.833),
                ('E1_AIRPATH', 4250500.0), ('E2_AIRPATH', 3680300.0),
                ('W1_AIRPATH', 1842354.0), ('W2_AIRPATH', 2405131.0),
                ('S1_DL', 1.0), ('S2_DL', 1.0), ('E1_DL', 1.0),
                ('E2_DL', 1.0), ('W1_DL', 1.0), ('W2_DL', 1.0)];

        # Set them
        for name,value in n:
            if name not in params:
                logging.info ('Cannot setup '+name);
            else:
                vector[params.index(name)] = value;
    else:
        logging.info ('Initialising degrees of freedom from file:   '+ file);
        if mode == 'beams' or Ltime == 'Light':
            print('We are here -------------------------------------------')
            print(params)
            # Some predefined quantities.
            n = [('S1_AIRPATH', 0.0), ('S2_AIRPATH', 573633.833),
                ('E1_AIRPATH', 4250500.0), ('E2_AIRPATH', 3680300.0),
                ('W1_AIRPATH', 1842354.0), ('W2_AIRPATH', 2405131.0),
                ('S1_DL', 1.0), ('S2_DL', 1.0), ('E1_DL', 1.0),
                ('E2_DL', 1.0), ('W1_DL', 1.0), ('W2_DL', 1.0),
                ('S1_BEAM1', 419584.274), ('S1_BEAM2', 210779.846),('S1_BEAM3', 0.0), ('S1_BEAM4', -210317.016),('S1_BEAM5', -420906.067), ('S1_BEAM6', -630805.373),
                ('S2_BEAM1', 419584.274), ('S2_BEAM2', 209369.004),('S2_BEAM3', 0.0), ('S2_BEAM4', -210760.772),('S2_BEAM5', -420354.843), ('S2_BEAM6', -631273.985),
                ('E1_BEAM1', 419809.997), ('E1_BEAM2', 210045.978),('E1_BEAM3', 0.0), ('E1_BEAM4', -210669.979),('E1_BEAM5', -420954.973), ('E1_BEAM6', -631390.929),
                ('E2_BEAM1', 418909.967), ('E2_BEAM2', 209645.987),('E2_BEAM3', 0.0), ('E2_BEAM4', -210769.981),('E2_BEAM5', -421355.009), ('E2_BEAM6', -631841.004),
                ('W1_BEAM1', 420409.977), ('W1_BEAM2', 210145.980),('W1_BEAM3', 0.0), ('W1_BEAM4', -210149.989),('W1_BEAM5', -420255.005), ('W1_BEAM6', -630540.967),
                ('W2_BEAM1', 419609.994), ('W2_BEAM2', 210245.997),('W2_BEAM3', 0.0), ('W2_BEAM4', -210469.961),('W2_BEAM5', -420654.982), ('W2_BEAM6', -630841.017)];
        else:
            # Some predefined quantities.
            n = [('S1_AIRPATH', 0.0), ('S2_AIRPATH', 573633.833),
                ('E1_AIRPATH', 4250500.0), ('E2_AIRPATH', 3680300.0),
                ('W1_AIRPATH', 1842354.0), ('W2_AIRPATH', 2405131.0),
                ('S1_DL', 1.0), ('S2_DL', 1.0), ('E1_DL', 1.0),
                ('E2_DL', 1.0), ('W1_DL', 1.0), ('W2_DL', 1.0)];
       
        # Set them
        for name,value in n:
            if name not in params:
                logging.warning ('Cannot setup '+name);
            else:
                vector[params.index(name)] = value;
        # Loop on parameters
        for name in params:

            # Get value
            if name[3:] == 'DL':
                value = 1.0;
            else:
                value = get_in_solution (file,name[0:2],name[3:]);

            # Set value
            if np.isnan(value):
                a=1#logging.info ('Cannot setup '+name);
            else:
                if verbose: logging.info ('Set '+name+' to %f'%value);
                File_Params.append(name)
                vector[params.index(name)] = value;
            

    logging.info ('Initialising constant degrees of freedom');
            
    # Set the DOF of the fit
    dofs = list (np.copy (params));

    # Remove AIRPATH and DL
    dofs = [d for d in dofs if 'DL' not in d];
    dofs = [d for d in dofs if 'AIRPATH' not in d];
    
    for d in File_Params:
        if d in dofs: dofs.remove (d);

    # S1 is reference of position and opds
    for d in ['S1_XOFFSET','S1_YOFFSET','S1_ZOFFSET','S1_LIGHT']:
        if d in dofs: dofs.remove (d);

    if dof == 'optimal':
        # BEAM3 is reference of beams for all telescope
        dofs = [d for d in dofs if '_BEAM3' not in d];

    if mode == 'beams' or Ltime == 'Light':
        dofs = [d for d in dofs if '_BEAM' not in d];
    if dof == 'iphase':
        dofs = [d for d in dofs if '_LIGHT' not in d];
    
    # The reference POP depends on the telescope
    for d in ['S1_POP1','S2_POP2','E1_POP1',
              'E2_POP3','W1_POP3','W2_POP5']:
        if d in dofs: dofs.remove (d);
    #Remove swash terms for S1
    for d in ['S1_EC','S1_SAC','S1_SAS']:
        if d in dofs: dofs.remove (d);

    if Ltime == 'Light':
        for i in range(55000,60000):
            for d in ['S1_LIGHT'+'_'+str(i)]:
                if d in dofs : 
                    dofs.remove (d);
    return vector, dofs;

def solve (model, params, vector, dof, old_solution, dof_mode):
    '''
    MODEL (nobs, nparams) is the input model of the inteferometer,
    constructed from the list of observation.

    PARAMS (nparams) is the list of parameters of this model, that
    is the name of each of them.

    VECTOR (nparams) is the value of these parameters. Note that
    since this is a linear system, the starting point is not
    important. We only need to make sure that the non-fitted
    parameters are properly filled.

    DOF is the list of free DOF, given as a list of name.

    Formally, the solution is obtained by solving the linear
    system MODEL x VECTOR = 0 Therefore the solution does not
    depend on the initial guess. However, the non-fitted parameters
    should be pre-filled.
    '''
    logging.info ('Solve vector from model');

    # The DL and AIRPATH shall never be fitted, thus remove
    # them in case the user forgot to remove them.
    dof = [p for p in dof if 'DL' not in p];
    dof = [p for p in dof if 'AIRPATH' not in p];

    # Some quantities
    nobs, nparams = model.shape;
    ndof = len(dof);
    if len(params) != nparams: raise ValueError('Inconsistent model and params');
    if len(vector) != nparams: raise ValueError('Inconsistent model and vector');        

    # Init memory
    M = np.zeros ((nobs,ndof));
    B = np.zeros (nobs);

    # Loop on parameters to fill the matrix
    nfree,nfixed = 0,0;
    for p in params:
        if p in dof:
            if verbose: logging.info ('Free parameter %s'%p);
            nfree += 1;
            M[:,dof.index(p)] = model[:,params.index(p)];
        else:
            if verbose: logging.info ('Fixed parameter %s   (%e)'%(p,vector[params.index(p)]));
            nfixed += 1;
            B -= model[:,params.index(p)] * vector[params.index(p)];
    
    logging.info ('%i observations, %i free parameters, %i fixed parameter'%(nobs,nfree,nfixed));
    output = vector.copy ();
    error  = output * 0;
    
    if ndof > 0:
        # Solve the system M.X = B
        fit = np.linalg.lstsq (M, B,rcond=None);
        X = fit[0];
        
        # Number of poorly constrained (low singular values)
        nbad = np.sum (fit[3] / np.max(fit[3]) < 1e-5);
        logging.info ('%i unconstrained parameters'%nbad);

        # Set them back into a full parameter vector
        for p in dof:
            output[params.index(p)] = X[dof.index(p)];

        # Compute uncertainties
        B = np.random.normal (scale=10, size=(len(B),100)) + B[:,None];
        eX = np.std (np.linalg.lstsq (M, B,rcond=None)[0], axis=1);
        for p in dof: error[params.index(p)] = eX[dof.index(p)];

    # Compute residuals and rms
    residual = np.einsum ('op,p->o', model, output);
    logging.info ('residual = %.0f um rms'%np.std (residual));
    LIGHT_terms = []
    if dof_mode == 'iphase':
        f = open('Iphase_offsets.dat', 'w')
        Lines = []
        for i,j in enumerate(output):
            #print params[i],j
            if 'BEAM' in params[i]:
                Lines.append('{} {}\n'.format(params[i],j))
        f.writelines(Lines)
        f.close()
        if dof_mode == 'iphase':
            #LIGHT_terms = calc_LIGHT ('Iphase_offsets.dat',old_solution)
            LIGHT_terms = [0.0, 4077223.291, 11266891.865, 22676608.592, 27324001.149,  -10857994.286]
            LIGHT_terms = [0.0, 4095592.310, 11246323.958, 22680172.727, 27313258.252,  -10861822.094]
        os.system("rm Iphase_offsets.dat")
    return output, error, residual, LIGHT_terms;

def dump_vector_as_chara (params, vector, new_dof, LIGHT_terms, dof_mode, name='solution.txt'):
    '''
    Dump the solution in a format ressembling
    the one of CHARA
    '''
    logging.info ('Dump solution into file');

    import os;
    if os.path.exists (name): os.remove (name)
    f = open(name, 'w+');
    
    # For each telescope
    a = 0
    for t in ['S1','S2','E1','E2','W1','W2']:
        f.write ('#\n');
        f.write (t+'\n');
        
        # logging.info position and path
        for p in ['XOFFSET','YOFFSET','ZOFFSET','LIGHT','AIRPATH','POP1','POP2','POP3','POP4','POP5','BEAM1','BEAM2','BEAM3','BEAM4','BEAM5','BEAM6','EC','SAC','SAS']:
            if t+'_'+p not in params:
                #f.write ('%-15s %+15s\n'%(p,'?????'));
                if verbose:
                    logging.info ('Unknown parameters '+t+'_'+p);
            else:
                f.write ('%-15s %+15.1f\n'%(p,vector[params.index(t+'_'+p)]));
        for p in ['_BEAM1','_BEAM2','_BEAM3','_BEAM4','_BEAM5','_BEAM6']:
            if p not in params:
                #f.write ('%-15s %+15s\n'%(p,'?????')); 
                if verbose:
                    logging.info ('Unknown parameters '+p);
            else:
                f.write ('%-15s %+15.1f\n'%(p[1:],vector[params.index(p)]));
        for p in new_dof:
            if t+p not in params:
                f.write ('%-15s %+15s\n'%(p,'?????'));
                if verbose:
                    logging.info ('Unknown parameters '+t+p);
            else:
                f.write ('%-15s %+15.1f\n'%(p,vector[params.index(t+p)]));
        if dof_mode == 'iphase':
                f.write ('%-15s %+15.1f\n'%('LIGHT',LIGHT_terms[a]))
                a += 1



    f.close ();
    logging.info ('Saving results of fit to: '+name);

def plot_residuals (residual, aux, offsets, dof_mode,vis='False'):
    '''
    for i,j in enumerate(residual):
        if j >= 10000 or j <= -10000:
            print i*3,j#i*3
    quit()
    '''
    writer = []

    for i in residual:
        writer.append('{}\n'.format(i))

    f=open('residual.dat','w')
    f.writelines(writer)
    f.close()


    '''
    Plot a summary of the fit residual
    '''
    logging.info ('Results from fitting:');
    
    plt.figure ();
    
    # Plot versus observation number
    ax = plt.subplot (212);
    ax.plot (-np.array(residual),'x',label='python');
    ax.set_ylim((-5000,5000))
    ax.set_xlabel ('obs #');
    ax.set_ylabel ('um');
    '''
    El = []
    Az = np.degrees(aux.az)
    #El = np.array(aux.elev)
    for i in range(0,len(aux.elev),1):
        E = (np.pi/2-aux.elev[i])/np.pi*180
        if Az[i] <= 0.0:
            Az[i] = Az[i] + 360. 
        #print Az[i]
        if Az[i] >= 315. or Az[i] <= 25.:
            E = E + 10.
        El.append(E)
    '''

    # Plot versus alt/az
    ax = plt.subplot (221, projection='polar');
    ax.scatter (aux.az,(np.pi/2-aux.elev)/np.pi*180, c=np.abs(residual), alpha=0.1);
    #ax.scatter (aux.az,El, c=np.abs(residual), alpha=0.1);
    ax.set_rlim (0,70);
    ax.set_theta_direction(-1);
    ax.set_theta_zero_location ('N');
    ax.set_thetagrids ([0,45,90,135,180,225,270,315],labels=['N','45','W','135','S','225','E','315']);
    plt.title ('nobs = %i'%len(residual));
    
    # Sigma clipping statistic
    mean, med, std = sigma_clipped_stats (residual);
    rms = np.std (residual);

    logging.info ('rms (offset) = %.0f um'%np.std (aux.offset1 - aux.offset2));
    logging.info ('rms (res) = %.0f um'%rms);
    logging.info ('rms_clip (res) = %.0f um'%std);
    
    # Plot histogram
    ax = plt.subplot (233);
    ax.hist (residual, bins=20);
    ax.set_yscale ('log');
    ax.set_xlabel ('um');
    plt.title ('rms = %.0f um\n rms-clip = %.0f um'%(rms,std));
    #plt.savefig('Feb2019_optimal.png')
    plt.show()
    return std

def plot_days_and_star (residual, aux):
    '''
    Plot residual versus time, with
    colors for days and star. Mostly usefull in
    python interactive, to zoom-in
    '''
    logging.info ('Plot residuals with day and star colors');

    plt.figure ();

    time = aux[:,3]/24.0 + aux[:,4];
    day  = aux[:,4];
    star = aux.chara;

    ax = plt.subplot (211);
    plt.scatter (time,residual,c=star%10,s=30, alpha=0.5);
    ax.set_xlabel ('time [day]');
    ax.set_ylabel ('um (color = star)');

    ax = plt.subplot (212, sharex=ax, sharey=ax);
    plt.scatter (time,residual,c=(day%3)+1,s=30, alpha=0.5);
    ax.set_xlabel ('time [day]');
    ax.set_ylabel ('um (color = day)');

def plot_histograms (residual, aux):
    '''
    Plot histogram of residual
    versus stars and day
    '''

    plt.figure ();

    # Compute RMS for each star
    stars = list(set(aux.chara));
    rms_star = np.array ([np.std(residual[aux.chara==s]) for s in stars]);
    
    # Compute RMS for each days
    days  = list(set(aux[:,4]));
    rms_day = np.array ([np.std(residual[aux[:,4]==d]) for d in days]);
    
    # Plot
    ax = plt.subplot (211);
    ax.hist (rms_day, bins=20);
    ax.set_xlabel ('rms in day');
    
    ax = plt.subplot (212);
    ax.hist (rms_star, bins=20);
    ax.set_xlabel ('rms in star');

def get_in_solution (f,tel,par):
    # Load files
    lines = [l.strip() for l in open(f).readlines ()];
    lines = [l.split () for l in lines if l != '' and l[0] != '#'];
    try:
        # Find telescope
        t = lines[ [l[0] for l in lines].index(tel):];
        # Find parameter
        return float(t[[l[0] for l in t].index(par)][1]);
    except:
        return np.nan;

def dump_solution_diff (f1, f2, un=None):
    '''
    logging.info a comparison between two solution file
    '''

    logging.info (f1);
    logging.info (f2);

    params = ['XOFFSET','YOFFSET','ZOFFSET','LIGHT','AIRPATH'];
    params += ['BEAM1','BEAM2','BEAM3','BEAM4','BEAM5','BEAM6'];
    params += ['POP1','POP2','POP3','POP4','POP5','EC','EC','SAC','SAS'];

    for tel in ['S1','S2','E1','E2','W1','W2']:
        logging.info ('');
        logging.info (tel);
        for par in params:
            p1 = get_in_solution (f1,tel,par);
            p2 = get_in_solution (f2,tel,par);
            s = '%+.3f'%((p1-p2)*1e-3);

            try:
                e = ' +- %.3f '%(un[1][un[0].index(tel+'_'+par)]*1e-3);
            except:
                e = ' ';
            
            logging.info ('%-10s   %+15s%smm'%(par,s,e));


def solution_difference(file1,file2):
    tels = ['S1','S2','E1','E2','W1','W2']
    pars = ['XOFFSET','YOFFSET','ZOFFSET','LIGHT','AIRPATH','POP1','POP2','POP3','POP4','POP5','BEAM1','BEAM2','BEAM3','BEAM4','BEAM5','BEAM6','EC','SAS','SAC']

    dash = '-' * 60
    print(dash)
    print('{:<15s}{:>10s}{:>17s}{:>15s}'.format('Parameter',file1,file2,'Difference'))
    print(dash)
    print('')
    for i in tels:
        for j in pars:
            val1 = get_in_solution (file1,i,j)
            val2 = get_in_solution (file2,i,j)
            try:
                diff = int(val1-val2)
            except:
                diff = np.nan
            print('{:<15s}{:>10s}{:>17s}{:>15s}'.format(i+'_'+j,str(val1),str(val2),str(diff)))
        print('')
    logging.info ('Finished comparing models')


def calc_LIGHT (model_file,old_solution):   
    ####### NOT USED ######
    logging.info ('Calculating LIGHT terms the Iphase way, as described by Theo in readiphase.c')
    g = open(model_file,'r')
    searchlines = g.readlines()
    g.close()
    sol = old_solution
    S1_b = [get_in_solution(sol,'S1','BEAM1'),get_in_solution(sol,'S1','BEAM2'),get_in_solution(sol,'S1','BEAM3'),get_in_solution(sol,'S1','BEAM4'),get_in_solution(sol,'S1','BEAM5'),get_in_solution(sol,'S1','BEAM6')]
    S2_b = [get_in_solution(sol,'S2','BEAM1'),get_in_solution(sol,'S2','BEAM2'),get_in_solution(sol,'S2','BEAM3'),get_in_solution(sol,'S2','BEAM4'),get_in_solution(sol,'S2','BEAM5'),get_in_solution(sol,'S2','BEAM6')]
    E1_b = [get_in_solution(sol,'E1','BEAM1'),get_in_solution(sol,'E1','BEAM2'),get_in_solution(sol,'E1','BEAM3'),get_in_solution(sol,'E1','BEAM4'),get_in_solution(sol,'E1','BEAM5'),get_in_solution(sol,'E1','BEAM6')]
    E2_b = [get_in_solution(sol,'E2','BEAM1'),get_in_solution(sol,'E2','BEAM2'),get_in_solution(sol,'E2','BEAM3'),get_in_solution(sol,'E2','BEAM4'),get_in_solution(sol,'E2','BEAM5'),get_in_solution(sol,'E2','BEAM6')]
    W1_b = [get_in_solution(sol,'W1','BEAM1'),get_in_solution(sol,'W1','BEAM2'),get_in_solution(sol,'W1','BEAM3'),get_in_solution(sol,'W1','BEAM4'),get_in_solution(sol,'W1','BEAM5'),get_in_solution(sol,'W1','BEAM6')]
    W2_b = [get_in_solution(sol,'W2','BEAM1'),get_in_solution(sol,'W2','BEAM2'),get_in_solution(sol,'W2','BEAM3'),get_in_solution(sol,'W2','BEAM4'),get_in_solution(sol,'W2','BEAM5'),get_in_solution(sol,'W2','BEAM6')]
    S1_p = [get_in_solution(sol,'S1','POP1'),get_in_solution(sol,'S1','POP2'),get_in_solution(sol,'S1','POP3'),get_in_solution(sol,'S1','POP4'),get_in_solution(sol,'S1','POP5')]
    S2_p = [get_in_solution(sol,'S2','POP1'),get_in_solution(sol,'S2','POP2'),get_in_solution(sol,'S2','POP3'),get_in_solution(sol,'S2','POP4'),get_in_solution(sol,'S2','POP5')]
    E1_p = [get_in_solution(sol,'E1','POP1'),get_in_solution(sol,'E1','POP2'),get_in_solution(sol,'E1','POP3'),get_in_solution(sol,'E1','POP4'),get_in_solution(sol,'E1','POP5')]
    E2_p = [get_in_solution(sol,'E2','POP1'),get_in_solution(sol,'E2','POP2'),get_in_solution(sol,'E2','POP3'),get_in_solution(sol,'E2','POP4'),get_in_solution(sol,'E2','POP5')]
    W1_p = [get_in_solution(sol,'W1','POP1'),get_in_solution(sol,'W1','POP2'),get_in_solution(sol,'W1','POP3'),get_in_solution(sol,'W1','POP4'),get_in_solution(sol,'W1','POP5')]
    W2_p = [get_in_solution(sol,'W2','POP1'),get_in_solution(sol,'W2','POP2'),get_in_solution(sol,'W2','POP3'),get_in_solution(sol,'W2','POP4'),get_in_solution(sol,'W2','POP5')]
    Air = [get_in_solution(sol,'S1','AIRPATH'),get_in_solution(sol,'S2','AIRPATH'),get_in_solution(sol,'E1','AIRPATH'),get_in_solution(sol,'E2','AIRPATH'),get_in_solution(sol,'W1','AIRPATH'),get_in_solution(sol,'W2','AIRPATH')]
    Light_S2,Light_E1,Light_E2,Light_W1,Light_W2 = [],[],[],[],[]
    for i,line in enumerate(searchlines):
        line = line.split()
        if 'S1' in line[0]:
            p1 = int((line[0])[4:-11])
            p2 = int((line[0])[5:-10])
            b1 = int((line[0])[10:-5])
            b2 = int((line[0])[15:])
            if 'S2' in line[0]:
                Light_S2.append(-float(line[1]) + S1_b[b1-1] + S1_p[p1-1] + Air[0] - S2_b[b2-1] - S2_p[p2-1] - Air[1])
            if 'E1' in line[0]:
                Light_E1.append(-float(line[1]) + S1_b[b1-1] + S1_p[p1-1] + Air[0] - E1_b[b2-1] - E1_p[p2-1] - Air[2])
            if 'E2' in line[0]:
                Light_E2.append(-float(line[1]) + S1_b[b1-1] + S1_p[p1-1] + Air[0] - E2_b[b2-1] - E2_p[p2-1] - Air[3])
            if 'W1' in line[0]:
                Light_W1.append(-float(line[1]) + S1_b[b1-1] + S1_p[p1-1] + Air[0] - W1_b[b2-1] - W1_p[p2-1] - Air[4])
            if 'W2' in line[0]:
                Light_W2.append(-float(line[1]) + S1_b[b1-1] + S1_p[p1-1] + Air[0] - W2_b[b2-1] - W2_p[p2-1] - Air[5])

    logging.warning('These LIGHT calculations assume the AIRPATH, POP and BEAM terms are correct in previous model')
    logging.info ('Average S1_LIGHT '+str(0.0))
    logging.info ('Average S2_LIGHT '+str(np.mean(Light_S2)))
    logging.info ('Average E1_LIGHT '+str(np.mean(Light_E1)))
    logging.info ('Average E2_LIGHT '+str(np.mean(Light_E2)))
    logging.info ('Average W1_LIGHT '+str(np.mean(Light_W1)))
    logging.info ('Average W2_LIGHT '+str(np.mean(Light_W2)))

    return [0.0, np.mean(Light_S2), np.mean(Light_E1), np.mean(Light_E2), np.mean(Light_W1), np.mean(Light_W2)]
        
