import matplotlib.pyplot as plt;
import matplotlib as mpl
mpl.rcParams['image.interpolation'] = 'nearest';
mpl.rcParams['axes.grid'] = True;
mpl.rcParams['legend.framealpha'] = 0.5;
plt.close ();
import numpy as np;
import re;
import solver as opds;
import sys, getopt
import logging
import init as init


#Initialise the log and read in the options from config file
settings_in, settings_res = init.Setup()

for i in settings_in: 
	if i == '-i':
		logging.info ('input file = '+ settings_res[settings_in.index(i)])
		inputFile = settings_res[settings_in.index(i)]

#Set the optional extras
beam_mode = 'normal'
if settings_res[settings_in.index('-b')] == str(1):
	beam_mode = 'beams'
time_mode = 'None'
if settings_res[settings_in.index('-t')] == str(1):
	time_mode = 'Light'
old_solution = 'None'
if settings_res[settings_in.index('-o')] != str(0):
	old_solution = settings_res[settings_in.index('-o')]
ec_mode = 'None'
if settings_res[settings_in.index('-ec')] == str(1):
	ec_mode = 'True'
sac_mode = 'None'
if settings_res[settings_in.index('-sac')] == str(1):
	sac_mode = 'True'
sas_mode = 'None'
if settings_res[settings_in.index('-sas')] == str(1):
	sas_mode = 'True'


#Are they just visualising an already built model?
if settings_res[settings_in.index('-v')] != str(0): 
	model_file = settings_res[settings_in.index('-v')]
	logging.info ('Visualising the data')
	model, new_dof, params, aux, offsets = opds.load_model (inputFile,fit='dl',dof='optimal', zcoord='iphase');
	vector, dof = opds.init_vector (params,file=model_file);
	output, error, residual, LIGHT_terms = opds.solve (model, params, vector, dof, 'old_solution',  dof_mode='optimal');
	

	opds.dump_vector_as_chara (params, output,new_dof, LIGHT_terms, dof_mode='optimal', name='rehash.txt');
	#The divide by 2 allows us to show the mechanical offsets rather than the physical offset. This is the value used on sky
	opds.plot_residuals (residual/2, aux, offsets, dof_mode='optimal',vis='True');
	#plt.savefig('Test_model.png');
	#plt.show ();
	quit()

#Are they comparing two models?
if settings_res[settings_in.index('-c1')] != str(0):
	logging.info ('Attempting to compare 2 models')
	file1 =  settings_res[settings_in.index('-c1')]
	try:
		file2 =  settings_res[settings_in.index('-c2')]
	except:
		logging.error ('Expecting second model file with option -c2')
	opds.solution_difference(file1,file2)


#Options to filter the input data before use 
if settings_res[settings_in.index('-g')] == str(1):
	GAIAout = inputFile.strip('.dat')+'_GAIAfiltered.dat'
	logging.info ('Performing GAIA filtering of your input data')
	init.Filter_Data2(inputFile,GAIAout,'MIRC_only')



rms_all = []
#Does the fitting for the requested fitting procedure
if settings_res[settings_in.index('-m')] == 'full':
	init.Standard_Fit(inputFile,time_mode,'normal',ec_mode,sas_mode,sac_mode,'iphase','solution_step1_iphase.txt','residual_step1_iphase.png',None,old_solution)
	init.Standard_Fit(inputFile,time_mode,beam_mode,ec_mode,sas_mode,sac_mode,'optimal','solution_step2_dof.txt','residual_step2_dof.png','solution_step1_iphase.txt',old_solution)

elif settings_res[settings_in.index('-m')] == 'iphase':
	init.Standard_Fit(inputFile,time_mode,'normal',ec_mode,sas_mode,sac_mode,'iphase','solution_iphase.txt','residual_iphase.png',None,old_solution)

elif settings_res[settings_in.index('-m')] == 'optimal':
	init.Standard_Fit(inputFile,time_mode,beam_mode,ec_mode,sas_mode,sac_mode,'optimal','solution_optimal.txt','residual_optimal.png',None,old_solution)


#plt.clf()
#plt.plot(rms_all,linestyle='None',marker='o')
#plt.show()







