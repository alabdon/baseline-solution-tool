from tkinter import *
import matplotlib.pyplot as plt;
import matplotlib as mpl
mpl.rcParams['image.interpolation'] = 'nearest';
mpl.rcParams['axes.grid'] = True;
mpl.rcParams['legend.framealpha'] = 0.5;
plt.close ();
import numpy as np;
import re;
import solver as opds;
import sys, getopt
import logging
import init as init
from astropy.stats import sigma_clipped_stats;
import os
from astroquery.vizier import Vizier

class CreateToolTip(object):
    #create a tooltip for a given widget
    def __init__(self, widget, text='widget info'):
        self.widget = widget
        self.text = text
        self.widget.bind("<Enter>", self.enter)
        self.widget.bind("<Leave>", self.close)
    def enter(self, event=None):
        x = y = 0
        x, y, cx, cy = self.widget.bbox("insert")
        x += self.widget.winfo_rootx() + 25
        y += self.widget.winfo_rooty() + 20
        # creates a toplevel window
        self.tw = Toplevel(self.widget)
        # Leaves only the label and removes the app window
        self.tw.wm_overrideredirect(True)
        self.tw.wm_geometry("+%d+%d" % (x, y))
        label = Label(self.tw, text=self.text, justify='left',
                       background='white', relief='solid', borderwidth=1,
                       font=("Helvetica", "10", "normal"))
        label.pack(ipadx=1)
    def close(self, event=None):
        if self.tw:
            self.tw.destroy()


def compute():
	#Compute the solution
	inputFile = e1.get()
	model_file = e2.get()
	#Set up the parameters
	if beam_modeB.get() == 1:
		beam_mode = 'beams'
	else:
		beam_mode = 'None'
	if time_modeB.get() == 1:
		time_mode = 'Light'
	else:
		time_mode = 'None'
	if len((e4.get()).split(',')) != ['']:
		restricts = (e4.get()).split(',')
		logging.info ('Restricting to following baselines'+str(restricts))
	else:
		restricts = []
	if (e5.get()).split(',') != ['']:
		rejects = (e5.get()).split(',')
		logging.info ('Rejecting the follwoing scopes'+str(rejects))
	else:
		rejects = []
	logging.info ('Starting the fitting process.........')
	logging.info ('Fitting x,y,z position acording to '+dof_m.get()+' method')
	print ('-----------------------------------------------------------------')
	logging.info ('Loading file'+ inputFile)
	#Finally start the fitting routines
	model, new_dof, params, aux, offsets = opds.load_model (inputFile,restrict=restricts,reject=rejects,fit='dl',dof=dof_m.get(), zcoord=zcoord_m.get(), Ltime=time_mode, ec_mode=EC_mode.get(),sas_mode=SAS_mode.get(),sac_mode=SAC_mode.get());
	vector, dof = opds.init_vector (params, mode=beam_mode, Ltime=time_mode, dof=dof_m.get(), file=model_file);
	output, error, residual, LIGHT_terms = opds.solve (model, params, vector, dof, model_file, dof_m.get());
	opds.dump_vector_as_chara (params, output,new_dof, LIGHT_terms, dof_m.get(), name='solution_'+dof_m.get()+'.txt');
	rms_a = opds.plot_residuals (residual, aux, offsets, dof_mode=dof_m.get());
	return rms_a

def vis_mod():
	#Visulaise a model, all the parameters are fixed, predicts the on-sky offsets
	inputFile = e1.get()
	model_file = e2.get()
	#Setting up the parameters
	if len((e4.get()).split(',')) != ['']:
		restricts = (e4.get()).split(',')
		logging.info ('Restricting to following baselines'+str(restricts))
	else:
		restricts = []
	if (e5.get()).split(',') != ['']:
		rejects = (e5.get()).split(',')
		logging.info ('Rejecting the follwoing scopes'+str(rejects))
	else:
		rejects = []
	#Computing the sythetic offsets
	model, new_dof, params, aux, offsets = opds.load_model (inputFile,restrict=restricts,reject=rejects,fit='dl',dof='optimal', zcoord='iphase')
	vector, dof = opds.init_vector (params,file=model_file);
	output, error, residual, LIGHT_terms = opds.solve (model, params, vector, dof, 'old_solution',  dof_mode='optimal');
	opds.dump_vector_as_chara (params, output,new_dof, LIGHT_terms, dof_mode='optimal', name='rehash.txt');
	opds.plot_residuals (residual/2, aux, offsets, dof_mode='optimal',vis='True');

def comp_mod():
	#Compares the differences between 2 solutions
	opds.solution_difference(e2.get(),e3.get())

def filters():
	#Filters the data according to given values
	inputFile = e1.get()
	outputFile = f4.get()
	if MIRC_mode.get() == 1:
		Extra = 'MIRC_only'
	else:
		Extra = 'None'
	init.Filter_Data2(inputFile,outputFile,Extra,float(f1.get()),float(f2.get()),float(f3.get()))

#Setup the logging and print welcome screen
init.Setup(GUI=1)
#Start the GUI window
window = Tk() 
window.title('BESOT: BaselinE SOlution Tool')

#Define some labels here
l1 = Label(window, text="Input Data")
l1.grid(row=0,column=0)
l1_ttp = CreateToolTip(l1, "Standard baseline data file of type recorded by CHARA array post May 2018")
l2 = Label(window, text="Input Solution")
l2.grid(row=1)
l2_ttp = CreateToolTip(l2, "Existing solution fo standard types created by BESOT or readiphase.\nCan be used to compare with second model or visualise with input data.\nWhen fitting parameters found in this file will be fixed.\nMUST BE 'None' WHEN NOT USING A SOLUTION")
l3 = Label(window, text="Comparison Solution")
l3.grid(row=2)
l3_ttp = CreateToolTip(l3, "Second existing solution for comparison with above solution.\nCalculates difference in each term of model.")
l4 = Label(window, text="Restrict Scopes")
l4.grid(row=6)
l4_ttp = CreateToolTip(l4, "Comma seperated list of telescopes e.g. S1,W2 \nWill only fit/visualise observations contain scopes given")
l5 = Label(window, text="Reject Scopes")
l5.grid(row=7)
l5_ttp = CreateToolTip(l5, "Comma seperated list of telescopes e.g. S2,E1 \nWill not fit/visualise observations containing these scopes")

#The input data windows
inFiles = StringVar()
e1 = Entry(window, textvariable=inFiles)
e1.grid(row=0, column=1)
inFiles.set("All_2022_MIRCX.dat")
Files = StringVar()
e2 = Entry(window, textvariable=Files)
e2.grid(row=1, column=1)
Files.set("None")
e3 = Entry(window)
e3.grid(row=2, column=1)
e4 = Entry(window)
e4.grid(row=6, column=1)
e5 = Entry(window)
e5.grid(row=7, column=1)


#Drop down menues for dof and zcoord options
dof_list = ('iphase','optimal')
dof_m = StringVar(window)
dof_m.set('optimal')
dof_l = Label(window, text="DOF Mode")
dof_l.grid(row = 8, column = 0)
dof_option = OptionMenu(window,dof_m,*dof_list)
dof_option.grid(row=8, column=1)
dof_l_ttp = CreateToolTip(dof_l, "Choose how to define the degrees of freedom\nThe iphase way 'S1S2P35B14'\nThe optimal way, where dof=model parameters")

zcoord_list = ('consider_index','iphase')
zcoord_m = StringVar(window)
zcoord_m.set('consider_index')
Z_option = OptionMenu(window,zcoord_m,*zcoord_list)
Z_option.grid(row=9, column=1)
Z_l = Label(window, text="Z Coordinate")
Z_l.grid(row = 9, column = 0)
Z_l_ttp = CreateToolTip(Z_l, "Choose how to define the Z coodinate term\nThe iphase way simply cos(El)\nConsidering the refractive index of atmosphere")


#Check boxes for valious options
beam_modeB = IntVar()
Fix_B = Checkbutton(window, text = "Fix Beams", onvalue = 1, offvalue = 0,variable=beam_modeB)
Fix_B.grid(row=4, column=0)
Fix_B_ttp = CreateToolTip(Fix_B, "Fix the BEAM terms to default values")
time_modeB = IntVar()
Time_Light = Checkbutton(window, text = "Time Dependent Light", onvalue = 1, offvalue = 0,variable=time_modeB)
Time_Light.grid(row=4, column=1)
Time_Light_ttp = CreateToolTip(Time_Light, "Time dependent LIGHT term with 15 day period")
EC_mode = IntVar()
EC_B = Checkbutton(window, text = "EC terms", onvalue = 1, offvalue = 0,variable=EC_mode)
EC_B.grid(row=5, column=0)
EC_B_ttp = CreateToolTip(EC_B, "Include elevation swash cosine terms?")
SAS_mode = IntVar()
SAS_B = Checkbutton(window, text = "SAS terms", onvalue = 1, offvalue = 0,variable=SAS_mode)
SAS_B.grid(row=5, column=1)
SAS_B_ttp = CreateToolTip(SAS_B, "Include azimuthal swash sine terms?")
SAC_mode = IntVar()
SAC_B = Checkbutton(window, text = "SAC terms", onvalue = 1, offvalue = 0,variable=SAC_mode)
SAC_B.grid(row=5, column=2)
SAC_B_ttp = CreateToolTip(SAC_B, "Include azimuthal swash cosine terms?")

#Functional buttons to visualuse, compare and compute
Load_mod = Button(text = "Visualise Model", fg = "green",command=vis_mod).grid(row=1, column=2)
Comp_mod = Button(text = "Compare Models", fg = "green",command=comp_mod).grid(row=2, column=2)
Compute = Button(text = "Compute New Solution", fg = "red",command=compute).grid(row=10, column=1)

#The filtering section 
Filter_Label = Label(window, text="Data Filtering",height=2,font=("Helvetica", 15)).grid(row=11,column=1)

#Label and entry box for seperation limit
l6 = Label(window, text="Hipp-GAI seperation limit")
l6.grid(row=12)
l6_ttp = CreateToolTip(l6, "Compare the coordinates from CHARA catalgoue with those of GAIA DR2\nA seperation greater than limit rejects the star")
Label(window, text="arcsec J2000").grid(row=12,column=2)
HipG = StringVar()
f1 = Entry(window, textvariable=HipG)
f1.grid(row=12, column=1)
HipG.set("0.5")

#Label and entry box for PM limit
l7 = Label(window, text="Proper Motion limit")
l7.grid(row=13)
l7_ttp = CreateToolTip(l7, "Check the proper motion from GAIA DR2\nA proper motion greater than limit rejects the star")
Label(window, text="mas/yr").grid(row=13,column=2)
prop = StringVar()
f2 = Entry(window, textvariable=prop)
f2.grid(row=13, column=1)
prop.set("50.0")

#Label and entry box for magnitude limit
l8 = Label(window, text="Hipp-GAI magnitude limit")
l8.grid(row=14)
l8_ttp = CreateToolTip(l8, "Compare the Vmag from CHARA catalgoue with the Gmag of GAIA DR2\nA difference greater than limit rejects the star")
Mag = StringVar()
f3 = Entry(window, textvariable=Mag)
f3.grid(row=14, column=1)
Mag.set("0.5")

#Label and entry box for output file name
l9 = Label(window, text="Filter Output")
l9.grid(row=15)
l9_ttp = CreateToolTip(l9, "Name of file to output filtered observations to")
Fil = StringVar()
f4 = Entry(window, textvariable=Fil)
f4.grid(row=15, column=1)
Fil.set("GAIAfiltered_Data.dat")

#Check box for MIRC-X filter options
MIRC_mode = IntVar()
MIRC_B = Checkbutton(window, text = "MIRC-X Only?", onvalue = 1, offvalue = 0,variable=MIRC_mode)
MIRC_B.grid(row=16, column=1)
MIRC_B_ttp = CreateToolTip(MIRC_B, "Limit filtered data to MIRC-X only?")

#Buttons to filter input and close window
Filt = Button(text = "Filter Input", fg = "red",command=filters).grid(row=18, column=1)
Close_GUI = Button(text = "Close", fg = "black",command=window.quit).grid(row=20, column=2)


window.mainloop()
















